const {role,users} = require('../models')

module.exports={

    // create user for auth
    createUser(createArgs) {
        return users.createUser(createArgs)
    },
    // find Email for auth
    findEmail (email){
        const user= users.findOne({
            where:{
                email,
            },
            include:{
                model:role,
            }
        })
        return user
    },
    // update to admin for auth
    async updateToAdmin (user){
        const {email}= user
        await users.update({ roleId:2}, 
            {
            where:{email}
        })
    }

    
}