const {product, history} = require("../models");

module.exports={

    async createProduct(requestBody){
        return await product.create(requestBody);
    },

    getProduct (){
        return product.findAll({
            include:{
                model:history,
            },
        });
    },

    getProductForUser (){
        return product.findAll({
            attributes:{ exclude:["historyId"]},
            where:{}
        })
    },

    getProductById (id){
        return product.findOne({
           where:{id,} 
        })
    },

    async updateProduct(id, requestBody){
        return await product.update(requestBody,{
            where:{
                id,
            }
        })
    },

    async deleteProduct (id){
        return await product.update('',{
            where:{id}
        });
    },  
};

