const {users} = require ('../models')

module.exports={
    async getUserById (id){
        return await users.findOne(id);
    }
};
