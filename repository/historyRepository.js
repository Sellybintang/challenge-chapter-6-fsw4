const {history} = require('../models')

module.exports={
    // who made?
    async createBy(requestBody){
        return await history.create({
            createdBy: requestBody
        })
    },

    // who updated?
    async updatedBy(id, requestBody){
        return await history.update(
            {
                updatedBy: requestBody,
                    where:{id},
            }
        ) 
    },

    // who deleted?
    async deletedBy (id, requestBody){
        return await history.update(
            {
                deletedBy : requestBody,
                    where:{id},
            },
        )
    },

    

};