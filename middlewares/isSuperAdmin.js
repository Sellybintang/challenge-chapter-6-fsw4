const {users}=require('../models');

module.exports= async function(req,res,next){
    try{
        const user=await users.findByPk(req.user.id);
        if (user.roleId==3)
        return next();
        throw new Error();
    }
    catch (err){
        res.status(200).json({
            status: "FAIL",
            message:"can be only accessed by admin",
          });
    }
}