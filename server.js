const dotenv = require('dotenv')
dotenv.config()
const express = require("express");
const morgan = require('morgan')
const path = require("path");
const cors = require ('cors')

// our own module
const routes = require("./routes")
const port = 3000

// Intializations
const app = express();

// basic express configuration
app.locals.moment = require('moment')


// Middleware to Pars JSON
app.use(express.json())
app.use(cors())

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended:false }))


// Routes
app.use(routes)

// errorHandler
const errorHandler = require ('./middlewares/errorHandler')
const ApiError = require ('./utils/ApiError')
const httpStatus = require ('http-status')


//Server Run
app.listen(port, () => {
    console.log(`Server running on ${Date(Date.now)}`)
    console.log(`Server listening on PORT: ${port}`)
});
