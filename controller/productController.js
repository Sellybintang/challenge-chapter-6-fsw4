const productService = require('../service/productService')

module.exports={
  getProducts(req,res,next){
    productService
    .getProducts()
    .then((products)=>{
      res.status(200).json({
        status: "OK",
        message:"succes",
        data: products,
      });
    })
  },
  getProductForUser(req,res,next){
    productService
    .getProductForUser(req.body)
    .then((products)=>{
      res.status(200).json({
        status: "OK",
        message:"succes",
        data: products,
      });
    })
  },

  createProduct(req,res,next){
    productService
    .createProduct(req.body, req.user.id)
    .then((products)=>{
      res.status(200).json({
        status: "OK",
        message:"succes",
        data: products,
      });
    })
    .catch((err)=>{
      next(err);
    });
  },

  updateProducts(req,res,next){
    productService
    .updateProducts(req.body, req.user.id)
    .then((products)=>{
      res.status(200).json({
        status: "OK",
        message:"succes",
        data: products,
      });
    })
    .catch((err)=>{
      next(err);
    });
  },

  deleteProduct(req,res,next){
    productService
    .deleteProducts(req.body, req.params.id)
    .then(()=>{
      res.status(200).json({
        status: "OK",
        message:"Product Successfully Deleted",
      });
    })
    .catch((err)=>{
      next(err);
    });
  },

    
};