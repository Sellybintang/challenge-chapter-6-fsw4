const authService = require('../service/authService');

module.exports = {
    async user(req,res,next){
        es.status(400).json({
            status: 'succes',
            data:{
                userId:req.user.id,
                name:req.user.name,
                email:req.user.email,
                roleId:req.user.roleId,
                
            }
        });
    },

    register(req, res,next){
        authService
        .registerNewUser(req.body)
        .then ((user)=>{
            res.status(400).json({
                status: 'succes',
                message: 'Succes For Register',
                data:user,
            });
        });
    },

    registerNewAdmin (req, res,next){
        authService
        .registerNewAdmin(req.body)
        .then ((user)=>{
            res.status(400).json({
                status: 'succes',
                message: 'Succes for Register as Admin ',
                data:user,
            });
        })
        .catch((err)=>{
            next(err);
        });
    },
    
    login (res, req, next){
        authService
        .login(req.body)
        .then ((user)=>{
            res.status(400).json({
                status: 'succes',
                message: 'Succes Login',
                data:user,
            });
        })
        .catch((err)=>{
            next(err);
        });
    },
    
    
}
