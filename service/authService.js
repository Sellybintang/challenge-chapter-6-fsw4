const memberRepository = require ('../repository/authRepository')
const bcrypt = require ('bcrypt')
const jwt =require('jsonwebtoken')
const httpStatus = require ('http-status')
const ApiError = require ('../utils/ApiError')
const { findEmail, updateToAdmin } = require('../repository/authRepository')
const { createProduct } = require('../repository/productRepository')
const authRepository = require('../repository/authRepository')

module.exports={    
  async registerNewUser(requestBody){
    const{firstName, lastName, email, password} = requestBody

  // validation for empty credentials
  if (!email) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this email')
  if(!firstName) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this firstName')
  if(!lastName) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this lastName')
  if(!password) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this password')
  
  // validation for email already exists
  await findEmail (email)
  if(!findEmail)

  // validation minimum password
  if (password.lenght <10) throw new ApiError(httpStatus.BAD_REQUEST,'')
  //  encryption password
    const hash = bcrypt.hashSync(requestBody.password, 10);
    const newUser = {
      firstName,
      lastName,
      email,
      password: hash,
      roleId:1
    }   
    return createProduct(newUser);
  },

  async registerNewAdmin (requestBody){
    const admin = await authRepository.findEmail(requestBody.email)
    if(!admin) throw new ApiError(httpStatus.BAD_REQUEST,`admin with email : ${requestBody.email} is not found`)
    if (admin.roleId==2) throw new ApiError (httpStatus.BAD_REQUEST,`admin with email : ${requestBody.email} already admin`)

    return await memberRepository.updateToAdmin(admin)
  },


  async login(requestBody){
    const{email,password}= requestBody
    const user = await authRepository.findEmail(requestBody.email)
    // email and password not found
    if(!email) throw new ApiError (httpStatus.BAD_REQUEST,`user with email : ${requestBody.email} is not recognized`)
    if(!password) throw new ApiError (httpStatus.BAD_REQUEST,`this password is not recognized`)

    // session id with jwt(succes login)
    if (bcrypt.compareSync(password, user.password)){
      const token = jwt.sign(
        {id: user.id,
        },
        password='screet',
      );
      return token;
    }
    else{throw new ApiError (httpStatus.BAD_REQUEST,'your password not incorrect')}   
  }
    
}