const productRepository = require ('../repository/productRepository')
const historyRepository = require ('../repository/historyRepository')
const userRepository = require ('../repository/userRepository')
const ApiError = require('../utils/ApiError')
const httpStatus = require('http-status')
const { getProduct, getProductForId, deleteProduct } = require('../repository/productRepository')

module.exports={
    async createProduct(requestBody, userId){
      const {name, price, size}=requestBody 
      // validation data empty
      if (!name) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this name')
      if (!price) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this price')
      if (!size) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this size')

      //history of product data maker
      const createHistory = await history.create(userId);
      // get id for history table
      const historyId= createHistory.id;
      // compare user data and id history
      const historyProduct = {
        name,
        price,
        size,
        historyId,
      };
      // make new data history of product data maker
        return productRepository.createProduct(historyProduct);
    },

    async getproducts() {
      const products =await productRepository.getProduct();

      return products;
    },

    async getProductForUser (){
      const cars=await productRepository.getProductForUser();
      return cars;
    },

    async updateProducts(id,userId,productId){
      const {name, price, size}=requestBody
      // validation data empty
      if (!name) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this name')
      if (!price) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this price')
      if (!size) throw new ApiError(httpStatus.BAD_REQUEST,'please fill out this size')

      const upProd = {name, price, size};
      const upProduct = await productRepository.getProductById(productId);
      if (!upProduct)throw new ApiError(httpStatus.BAD_REQUEST,'your id not found')
      history.updateBy(userId.upProduct.historyId);
      await productRepository. updateProduct(productId.upProd);
      return productRepository.getProductById(productId);
    },

    async deleteProducts (userId, productId) {
      const products = await productRepository.getProductById(productId);
      if (!products)throw new ApiError(httpStatus.BAD_REQUEST,'your id not found')
      history.deleteProducts(userId.products.historyId);

      return await productRepository.delete(productId);
      },
      
}