'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert ("roles",[
      {
        id : 1,
        name: "member",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id : 2,
        name: "Admin",
        createdAt: new Date(),
        updatedAt: new Date(),

      },
      {
        id : 3,
        name: "Super Admin",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
