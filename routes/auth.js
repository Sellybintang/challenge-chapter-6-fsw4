const router = require('express').Router();
const authenticate=require('../middlewares/authenticate')
const isSuperAdmin=require('../middlewares/isSuperAdmin')
const{user,register, registerNewAdmin, login }=require('../controller/authController')

// API auth
router.post('/register',register)
router.post('/login',login)
router.post('/registerNewAdmin',authenticate, isSuperAdmin, registerNewAdmin)
router.get('/user',authenticate, user )

