const router = require('express').Router()
// Controller
const Product = require('../controller/productController')

// middleware
const isAdmin=require('../middlewares/isAdmin')
const authenticate = require('../middlewares/authenticate')

// API server
router.post ('/addProduct', authenticate, Product.createProduct);router.get ('/', authenticate, Product.getProductForUser);
router.get ('/admin', authenticate, isAdmin,Product.getProducts);
router.put ('/update/:id', authenticate, Product.updateProducts);
router.delete ('/delete/:id', authenticate, Product.deleteProduct);

module.exports = router
